(module eping
  (eping 
   eping!)
  (import
    scheme
    chicken
    extras
    foreign 
    srfi-4
    srfi-18)
  (use
    socket
    posix)
  (include "eping-version.scm")
  (include "eping.scm"))
