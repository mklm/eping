;;; Copyright (c) 2013-2014
;;  Michele La Monaca (eping@lamonaca.net)
;;; All rights reserved.

(use eping)
(use srfi-18)
(use ip-tools)
(use data-structures)

(include "eping-version.scm")

(define *mode 'probe)
(define *quiet #f)
(define *count #f)
(define *interval 1000)
(define *timeout 1000)
(define *ttl 64)
(define *tos 0)
(define *src-addr #f)
(define *dont-fragment #f)
(define *id #f)
(define *seq-start 0)
(define *size 56)
(define *pattern "")
(define *recv-min #f)
(define *lost-max #f)
(define *cons-recv-min #f)
(define *cons-lost-max #f)
(define *msg1 #f)
(define *msg2 "")
(define *dotcols 0)
(define *version #f)

(define host #f)

(define (usage #!optional port)
  (with-output-to-port (or port (current-error-port))
    (lambda ()
      (print (substring "
eping HOST|HOST-LIST|HOST-RANGE|HOST-FILE
      [-mode probe|stats|dot|mtu]
      [-count COUNT]
      [-interval INTERVAL]
      [-timeout TIMEOUT]
      [-ttl TTL]
      [-tos TOS]
      [-src-addr IP]
      [-dont-fragment]
      [-id ID]
      [-seq-start SEQ]
      [-size SIZE]
      [-pattern PATTERN]
      [-recv-min RMIN]
      [-lost-max LMAX]
      [-cons-recv-min CRMIN]
      [-cons-lost-max CLMAX]
      [-msg1 MSG1]
      [-msg2 MSG2]
      [-dotcols COLS]
      [-quiet]
      [-version]"
                        1))))
  (exit 1))

(define (ping host mode msg1 quiet)
  (eping host mode:           mode
              count:         *count
              interval:      *interval
              timeout:       *timeout
              ttl:           *ttl
              tos:           *tos
              src-addr:      *src-addr
              dont-fragment: *dont-fragment
              id:            *id
              seq-start:     *seq-start
              size:          *size
              pattern:       *pattern
              recv-min:      *recv-min
              lost-max:      *lost-max
              cons-recv-min: *cons-recv-min
              cons-lost-max: *cons-lost-max
              msg1:           msg1
              msg2:          *msg2
              dotcols:       *dotcols
              quiet:          quiet))

(define (ping-range first-ip last-ip msg1)
  (let ((last-ip+1 (ip4-next last-ip)))
    (map thread-join!
         (let loop ((host first-ip))
           (thread-sleep! 0.01)
           (if (ip4=? host last-ip+1)
             '()
             (cons (thread-start! (lambda () (ping host 'probe msg1 #f)))
                   (loop (ip4-next host))))))))

(define (ping-list host-list msg1)
  (unless (null? host-list)
    (map thread-join!
         (let loop ((hosts host-list))
           (thread-sleep! 0.01)
           (if (null? hosts)
             '()
             (cons (thread-start! (lambda () (ping (car hosts) 'probe msg1 #f)))
                   (loop (cdr hosts))))))))

(define (ping-file p msg1)
  (unless (eof-object? (peek-char p))
    (map thread-join!
         (let loop ((host (read p)))
           (thread-sleep! 0.01)
           (if (eof-object? host)
             (begin
               (close-input-port p)
               '())
             (cons (thread-start! (lambda () (ping host 'probe msg1 #f)))
                   (loop (read p))))))))

(let loop ((args (command-line-arguments)))
  (define (arg->option arg)
    (string->symbol (string-append "*" (substring arg 1))))
  (unless (null? args)
    (define arg (car args))
    (cond ((string=? arg "-mode")
           (if (pair? (cdr args))
             (set! *mode (string->symbol (cadr args)))
             (usage))
           (if (memq *mode '(dot mtu probe stats))
             (loop (cddr args))
             (usage)))
          ((or (string=? arg "-quiet")
               (string=? arg "-version")
               (string=? arg "-dont-fragment"))
           (let ((option (arg->option arg)))
             (eval `(set! ,option #t)))
           (loop (cdr args)))
          ((or (string=? arg "-msg1")
               (string=? arg "-msg2")
               (string=? arg "-pattern")
               (string=? arg "-src-addr"))
           (if (pair? (cdr args))
             (let ((option (arg->option arg)))
               (eval `(set! ,option ,(cadr args))))
             (usage))
           (loop (cddr args)))
          ((or (string=? arg "-count")
               (string=? arg "-interval")
               (string=? arg "-timeout")
               (string=? arg "-dotcols")
               (string=? arg "-ttl")
               (string=? arg "-tos")
               (string=? arg "-id")
               (string=? arg "-seq-start")
               (string=? arg "-size")
               (string=? arg "-recv-min")
               (string=? arg "-lost-max")
               (string=? arg "-cons-recv-min")
               (string=? arg "-cons-lost-max"))
           (if (pair? (cdr args))
             (let ((option (arg->option arg))
                   (value (string->number (cadr args))))
               (if value
                 (eval `(set! ,option ,value))
                 (usage)))
             (usage))
           (loop (cddr args)))
          (else
           (if (or host (eq? #\- (string-ref arg 0)))
             (usage)
             (begin
               (set! host arg)
               (loop (cdr args))))))))

(when *version
  (print "eping v" eping-version)
  (exit 0))
(unless host
  (usage (current-output-port)))
(unless (or (not *src-addr) (ip4? *src-addr))
  (with-output-to-port (current-error-port)
    (lambda ()
      (print "Invalid src-addr: " *src-addr)))
  (exit 1))

(cond ((string-index host #\/)
       (let* ((ip/net (ip4-split host))
              (first-ip (ip4-hostmin (car ip/net) (cdr ip/net)))
              (last-ip (ip4-hostmax (car ip/net) (cdr ip/net)))
              (msg1 (or *msg1 "%i\n")))
         (ping-range first-ip last-ip msg1)))
      ((string-index host #\-) =>
       (lambda (i)
         (let ((first-ip (substring host 0 i))
               (last-ip (substring host (+ 1 i)))
               (msg1 (or *msg1 "%i\n")))
           (if (ip4>? first-ip last-ip)
             (begin
               (display "invalid IP range\n" (current-error-port))
               (exit 1))
             (ping-range first-ip last-ip msg1)))))
      ((string-index host #\,)
       (let ((msg1 (or *msg1 "%h\n")))
         (ping-list (string-split host ",") msg1)))
      ((char=? #\@ (string-ref host 0))
       (let ((p (open-input-file host))
             (msg1 (or *msg1 "%h\n")))
         (ping-file p msg1)))
      (else
       (let ((msg1 (or *msg1 "%h is alive\n")))
         (if (ping host *mode msg1 *quiet)
           (exit 0)
           (exit 1)))))
