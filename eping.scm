;;; Copyright (c) 2013-2014
;;; Michele La Monaca (eping@lamonaca.net)
;;; All rights reserved.

;;;
;;  ICMP / Low-level
;;;

(cond-expand
  (little-endian
    (foreign-declare "#define eping_ntohs(x) (_tmp=(x), \\
                                                ((_tmp>>8) & 0xff) | \\
                                                ((_tmp<<8) & 0xff00))"))
  (else
    (foreign-declare "#define eping_ntohs(x) (x)")))

(define inet-checksum
  (foreign-lambda* unsigned-short ((u16vector p) (unsigned-short len))
"   unsigned short _tmp;
    register long sum = 0;
    register unsigned short checksum;
    while (len > 1) { sum += eping_ntohs(*p++); len -= 2; }
    sum  = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    checksum = ~sum;
    C_return(checksum);
"
))

(define (set-ip-header so tos ttl dont-fragment)
  (set! (ip-time-to-live so) ttl)
  (set! (ip-type-of-service so) tos)
  (if dont-fragment
    (cond ((eq? (software-version) 'freebsd)
           (set-socket-option so ipproto/ip 67 #t))
          ((eq? (software-type) 'windows)
           (set-socket-option so ipproto/ip 14 #t))
          (else
            (set-socket-option so ipproto/ip ip/hdrincl #t)))))

(define (make-ip-header dst-ip tos ttl len)
  (define total-len (short->netbytes (+ 20 len)))
  (define flg-fragm (string #\x40 #\x00))
  (set! tos (integer->char tos))
  (set! ttl (integer->char ttl))
  (when (eq? (software-version) 'macosx)
    (set! total-len (string (string-ref total-len 1) (string-ref total-len 0)))
    (set! flg-fragm (string (string-ref flg-fragm 1) (string-ref flg-fragm 0))))
  (string-append (string #\x45  tos ) total-len                                  ;; ver ihl tos total-len
                 (string #\x00 #\x00) flg-fragm                                  ;; identification flags frags
                 (string  ttl  #\x01 #\x00 #\x00                                 ;; ttl protocol checksum
                         #\x00 #\x00 #\x00 #\x00)                                ;; ip-src
                 (aton   dst-ip)))                                               ;; ip-dst

;; struct timeval format
(define (make-timestamp-opt)
  (let* ((tv-s (current-seconds))
         (tv-u (modulo (* (current-milliseconds) 1000) 1000000))
         (q-s (quotient tv-s 65536))
         (m-s (modulo tv-s 65536))
         (q-u (quotient tv-u 65536))
         (m-u (modulo tv-u 65536)))
    (string (integer->char (inexact->exact (quotient q-s 256)))
            (integer->char (inexact->exact (modulo   q-s 256)))
            (integer->char (inexact->exact (quotient m-s 256)))
            (integer->char (inexact->exact (modulo   m-s 256)))
            (integer->char (inexact->exact (quotient q-u 256)))
            (integer->char (inexact->exact (modulo   q-u 256)))
            (integer->char (inexact->exact (quotient m-u 256)))
            (integer->char (inexact->exact (modulo   m-u 256))))))

(define (rtt-calc buf ts)
  (let ((now-s  (car ts))
        (now-u  (modulo (* (cadr ts) 1000) 1000000))
        (then-s (+ (* 16777216 (char->integer (string-ref buf 0)))
                   (*    65536 (char->integer (string-ref buf 1)))
                   (*      256 (char->integer (string-ref buf 2)))
                               (char->integer (string-ref buf 3))))
        (then-u (+ (*    65536 (char->integer (string-ref buf 5)))
                   (*      256 (char->integer (string-ref buf 6)))
                               (char->integer (string-ref buf 7)))))
    (inexact->exact
      (floor (+ (* (- now-s then-s) 1000) (/ (- now-u then-u) 1000))))))

(define (icmp-checksum type code payload)
  (if (odd? (string-length payload))
    (set! payload (string-append payload (string #\nul))))
  (let ((blob-pseudo-packet
          (string->blob (string-append
                          (string (integer->char type)
                                  (integer->char code) #\nul #\nul)
                          payload))))
    (let ((sum (inet-checksum (blob->u16vector blob-pseudo-packet)
                              (+ 4 (string-length payload)))))
      (short->netbytes sum))))

(define (icmp-packet type code payload)
  (string-append (string (integer->char type) (integer->char code))
                 (icmp-checksum type code payload)
                 payload))

(define (icmp-echo-request id seq timestamp payload)
  (if timestamp (set! timestamp (make-timestamp-opt)) "")
  (let ((icmp-partial-packet (make-string 0)))
    (set! icmp-partial-packet (string-append (short->netbytes id)
                                             (short->netbytes seq)
                                             timestamp payload))
    (icmp-packet 8 0 icmp-partial-packet)))

(define (icmp-match-request buf icmp-request ip)
  (let ((ihl (* 4 (- (char->integer (string-ref buf 0)) 64))))
    (if (and (string=? (ntoa (substring buf (+ ihl 8 16) (+ ihl 8 20)))
                       ip)
             (string=? (substring buf (+ ihl 8 20) (+ ihl 8 28))
                       (substring icmp-request 0 8)))
      #t #f)))

(define (icmp-decode buf)
  (let ((ihl (* 4 (- (char->integer (string-ref buf 0)) 64))))
    (cons (char->integer (string-ref buf ihl))
          (char->integer (string-ref buf (+ 1 ihl))))))

(define (icmp-time-exceeded? buf)
  (and-let* ((ihl (* 4 (- (char->integer (string-ref buf 0)) 64)))
             ((= (char->integer (string-ref buf ihl)) 11))
             ((= (char->integer (string-ref buf (+ 1 ihl))) 0)))
    (ntoa (substring buf 12 16))))

(define (icmp-unreachable? buf)
  (and-let* ((ihl (* 4 (- (char->integer (string-ref buf 0)) 64)))
             ((= (char->integer (string-ref buf ihl)) 3)))
    (char->integer (string-ref buf (+ 1 ihl)))))

(define (icmp-echo-request? buf #!optional (id #f) (seq #f) (src-ip #f))
  (and-let* ((ihl (* 4 (- (char->integer (string-ref buf 0)) 64)))
             ((= (char->integer (string-ref buf ihl)) 8))
             (r-seq (+ (* 256 (char->integer (string-ref buf (+ 6 ihl))))
                       (char->integer (string-ref buf (+ 7 ihl)))))
             (r-id  (+ (* 256 (char->integer (string-ref buf (+ 4 ihl))))
                       (char->integer (string-ref buf (+ 5 ihl)))))
             ((or (not id) (= r-id id)))
             ((or (not seq) (= r-seq seq)))
             ((or (not src-ip) (string=? (ntoa (substring buf 12 16))
                                         src-ip))))))

(define (icmp-echo-replay? buf #!optional (ts #f)
                                          (id #f)
                                          (seq #f)
                                          (src-ip #f)
                                          (sent-buf #f))
  (and-let* ((ihl (* 4 (- (char->integer (string-ref buf 0)) 64)))
             (ttl (char->integer (string-ref buf 8)))
             ((= (char->integer (string-ref buf ihl)) 0))
             ((= (char->integer (string-ref buf (+ 1 ihl))) 0))
             (r-seq (+ (* 256 (char->integer (string-ref buf (+ 6 ihl))))
                       (char->integer (string-ref buf (+ 7 ihl)))))
             (r-id  (+ (* 256 (char->integer (string-ref buf (+ 4 ihl))))
                       (char->integer (string-ref buf (+ 5 ihl)))))
             ((or (not id) (= r-id id)))
             ((or (not seq) (= r-seq seq)))
             ((or (not src-ip) (string=? (ntoa (substring buf 12 16)) src-ip)))
             ((or (not sent-buf) (string=? buf sent-buf))))
    (if ts
      (vector ihl ttl r-id r-seq (rtt-calc (substring buf (+ 8 ihl) (+ 16 ihl))
                                           ts))
      (vector ihl ttl r-id r-seq #f))))
;;;

;;;
;;  Helper/Misc functions
;;;
(define (aton ip)
  (define v (make-string 4))
  (and (< 0 (string-length ip) 16)
       (let loop ((i 0) (num-start-pos 0) (byte 1))
         (if (or (= i (string-length ip)) (char=? (string-ref ip i) #\.))
           (and (< byte 5)
                (let* ((num-str (substring ip num-start-pos i))
                       (num (string->number num-str)))
                  (string-set! v (- byte 1) (integer->char num))
                  (and num
                       (<= 0 num 255)
                       (or (= (string-length num-str) 1)
                           (not (char=? #\0 (string-ref num-str 0))))
                       (if (= i (string-length ip))
                         (and (= byte 4) v)
                         (loop (+ i 1) (+ i 1) (+ byte 1))))))
           (and (char-numeric? (string-ref ip i))
                (loop (+ i 1) num-start-pos byte))))))

(define (ntoa ip)
  (string-append (number->string (char->integer (string-ref ip 0))) "."
                 (number->string (char->integer (string-ref ip 1))) "."
                 (number->string (char->integer (string-ref ip 2))) "."
                 (number->string (char->integer (string-ref ip 3)))))

(define (short->netbytes w)
  (string (integer->char (quotient w 256))
          (integer->char (modulo w 256))))

(define (hostname->ip4 host)
  (if (symbol? host)
    (set! host (symbol->string host)))
  (let ((addr-list (address-information host #f family: af/inet)))
    (if (pair? addr-list)
      (sockaddr-address (addrinfo-address (car addr-list)))
      #f)))

(define (create-pattern size base)
  (let ((pattern "") (patter-len (string-length base))
        (tsize (if (< size 8) (+ size 8) size)))
    (if (= patter-len 0)
      (let loop ((ch 8))
        (when (< ch tsize)
          (set! pattern (string-append pattern (string (integer->char ch))))
          (if (< (+ ch 1) tsize) (loop (+ ch 1)))))
      (let loop ((i 8))
        (if (<= (+ i patter-len) tsize)
          (set! pattern (string-append pattern (substring base 0 patter-len)))
          (set! pattern (string-append pattern (substring base 0 (- tsize i)))))
        (if (< (+ i patter-len) tsize) (loop (+ i patter-len)))))
    pattern))

(define (make-msg msg host ip)
  (let loop ((i 0) (msg msg))
    (if (>= (+ i 1) (string-length msg))
      msg
      (if (and (char=? #\% (string-ref msg i))
               (< i (string-length msg))
               (or (= i 0) (not (char=? #\\ (string-ref msg (- i 1))))))
        (let ((ch (string-ref msg (+ i 1))))
          (cond ((char=? ch #\i)
                 (loop i (string-append (substring msg 0 (- i 0)) ip
                                        (substring msg (+ i 2)))))
                ((char=? ch #\h)
                 (loop i (string-append (substring msg 0 (- i 0)) host
                                        (substring msg (+ i 2)))))
                ((char=? ch #\n)
                 (loop i (string-append (substring msg 0 (- i 0)) "\n"
                                        (substring msg (+ i 2)))))
                ((char=? ch #\t)
                 (loop i (string-append (substring msg 0 (- i 0)) "\t"
                                        (substring msg (+ i 2)))))
                (else (loop (+ i 1) msg))))
        (loop (+ i 1) msg)))))

(define (print-stats-rcv host received sent)
  (let ((loss-perc (/ (floor (* (/ (- sent received) sent) 10000)) 100)))
    (newline)
    (print "--- " host " ping statistics ---")
    (print sent " packets transmitted, "
           received " packets received, "
           loss-perc "% packet loss")))

(define (print-stats-rtt rtt-min rtt-avg rtt-max rtt-std)
  (print "round-trip min/avg/max/stddev = "
         (exact->inexact rtt-min)
         "/" (/ (floor (* 100 rtt-avg)) 100)
         "/" (exact->inexact rtt-max)
         "/" (/ (floor (* 100 rtt-std)) 100) " ms"))
;;;

;;;
;;  eping function
;;;

(define-syntax eping!
  (syntax-rules ()
    ((_ h)
     (let ((t (symbol->string 'h)))
       (eping t)))
    ((_ h p v ...)
     (let ((t (symbol->string 'h)))
       (eping t 'p v ...)))))

(define (eping host #!key (mode 'probe)
                          (count #f)
                          (timeout 1000)
                          (interval 1000)
                          (ttl 64)
                          (tos 0)
                          (dont-fragment #f)
                          (src-addr #f)
                          (id #f)
                          (seq-start 0)
                          (size 56)
                          (pattern "")
                          (recv-min #f)
                          (lost-max #f)
                          (cons-recv-min #f)
                          (cons-lost-max #f)
                          (msg1 "%i is alive%n")
                          (msg2 "")
                          (dotcols 0)
                          (quiet #t))
  (unless (memq mode '(dot mtu probe stats))
    (abort 'exn-eping-unsupported-mode))
  (if (< interval timeout)
    (set! timeout interval))
  (if (and (eq? mode 'probe)
           (not recv-min)
           (not lost-max)
           (not cons-recv-min)
           (not cons-lost-max))
    (set! recv-min 1))
  (if (eq? mode 'mtu)
    (begin
      (set! size 40)
      (set! dont-fragment #t)
      (if (not count) (set! count 64)))
    (if (not count) (set! count 5)))
  (if (symbol? host)
    (set! host (symbol->string host)))
  (let ((ip host))
    (unless (aton host)
      (set! ip (hostname->ip4 host)))
    (if (not ip)
      (begin
        (unless quiet
          (display (string-append "Unable to resolve hostname '" host "'\n")
                   (current-error-port)))
        #f)
      (let ((repeated-pattern (create-pattern size pattern))
            (icmp-packet "")
            (so (socket af/inet sock/raw ipproto/icmp))
            (addr (inet-address ip #f))
            (buf (make-string 65535))
            (sent 0)
            (received 0)
            (c-received 0)
            (c-lost 0)
            (rtt-min timeout)
            (rtt-max 0)
            (rtt-tot 0)
            (rtt2-tot 0)
            (mtu 0)
            (double-mtu #f)
            (mtu-step 256)
            (mtu-hits 0)
            (dot-string ""))
        (set! msg1 (make-msg msg1 host ip))
        (set! msg2 (make-msg msg2 host ip))
        (socket-receive-timeout timeout)
        (if src-addr (socket-bind so (inet-address (if (symbol? src-addr)
                                       (symbol->string src-addr) src-addr)
                                       #f)))
        (set-ip-header so tos ttl dont-fragment)
        (unless id
          (set! id (+ (random 32768) (random 32768))))
        (set-signal-handler! signal/int (lambda (sig) (set! count 1)))
        (if (and (eq? mode 'stats) (not quiet))
          (set-signal-handler! (if (eq? (build-platform) 'mingw32)
                                 21
                                 signal/quit)
                               (lambda (sig)
                                 (print-stats-rcv host received sent)
                                 (and-let* (((> received 0))
                                            (rtt-avg (/ rtt-tot received))
                                            (rtt-std
                                              (sqrt (- (/ rtt2-tot received)
                                                       (* rtt-avg rtt-avg)))))
                                   (print-stats-rtt rtt-min rtt-avg
                                                    rtt-max rtt-std)
                                   (newline)))))
        ;; main loop
        (let Loop ((seq seq-start)
                   (expired #f)
                   (replied #f)
                   (prev-replied #f)
                   (icmp-size 0)
                   (rcv-ttl 0)
                   (rtt 0))
          (set! icmp-packet (icmp-echo-request id seq #t repeated-pattern))
          (handle-exceptions exn-message-too-long
                               (set! expired #t)
                               (if (ip-header-included? so)
                                 (socket-send-to
                                   so
                                   (string-append (make-ip-header ip tos ttl
                                                    (string-length icmp-packet))
                                                  icmp-packet)
                                   addr)
                                 (socket-send-to so icmp-packet addr)))
          (handle-exceptions exn-timeout
                               (set! expired #t)
                               (let loop ((rcv-size 0) (ts '()))
                                 (set! rcv-size (socket-receive! so buf))
                                 (set! ts (list (current-seconds)
                                                (current-milliseconds)))
                                 (cond ((icmp-echo-replay? buf ts id seq ip) =>
                                        (lambda (reply-info)
                                          (set! icmp-size (- rcv-size
                                                             (vector-ref
                                                               reply-info 0)))
                                          (set! rcv-ttl (vector-ref
                                                          reply-info 1))
                                          (set! rtt (vector-ref reply-info 4))
                                          (if (> rtt timeout)
                                            (set! expired #t)
                                            (set! replied #t))))
                                       ((icmp-echo-request? buf id seq ip)
                                        (loop 0 '()))                            ;; ignore self-request
                                       (else
                                         (let ((icmp-type (car
                                                            (icmp-decode buf))))
                                           (unless (and (memq icmp-type '(3 11))
                                                        (icmp-match-request
                                                          buf icmp-packet ip))
                                             (loop 0 '())))))))                  ;; ignore unrelated icmp packets
          (set! sent (+ 1 sent))
          (cond ((not expired)
                 (if replied
                   (begin
                     ;; echo reply arrived in time
                     (set! received (+ 1 received))
                     (if prev-replied
                       (set! c-received (+ 1 c-received))
                       (set! c-received 1))
                     (cond ((eq? mode 'dot)
                            (if quiet
                              (set! dot-string (string-append dot-string "."))
                              (begin
                                (display ".")
                                (if (and (> dotcols 0) (= 0 (modulo (+ 1 seq)
                                                                    dotcols)))
                                  (newline)
                                  (flush-output)))))
                           ((eq? mode 'mtu)
                            (if (= mtu size)
                              (if (= mtu-step 1)
                                (set! mtu-hits (+ mtu-hits 1)))
                              (begin
                                (set! mtu size)
                                (set! mtu-hits 1)))
                            (if double-mtu
                              (set! mtu-step (* 2 mtu-step))
                              (set! double-mtu #t))
                            (set! size (+ size mtu-step))
                            (set! repeated-pattern (create-pattern size
                                                                   pattern)))
                           ((eq? mode 'stats)
                            (if (and (> rtt 0) (< rtt rtt-min))
                              (set! rtt-min rtt))
                            (if (> rtt rtt-max) (set! rtt-max rtt))
                            (set! rtt-tot (+ rtt-tot rtt))
                            (set! rtt2-tot (+ rtt2-tot (* rtt rtt)))
                            (unless quiet
                              (print icmp-size
                                     " bytes from " ip
                                     ": icmp_seq=" seq
                                     " ttl=" rcv-ttl
                                     " time=" (if (= 0 rtt) "<1" rtt) "ms")))))
                   ;; not an echo reply
                   (cond ((eq? mode 'probe)
                          (if prev-replied
                            (set! c-lost 1)
                            (set! c-lost (+ 1 c-lost))))
                         ((icmp-time-exceeded? buf) =>
                          (lambda (sender-ip)
                            (cond ((eq? mode 'dot)
                                   (if quiet
                                     (set! dot-string
                                           (string-append dot-string "T"))
                                     (begin
                                       (display "T")
                                       (if (and
                                             (> dotcols 0)
                                             (= 0 (modulo (+ 1 seq) dotcols)))
                                         (newline)
                                         (flush-output)))))
                                  ((eq? mode 'stats)
                                   (unless quiet
                                     (print "icmp_seq="
                                            seq
                                            " ttl expired in transit at "
                                            sender-ip))))))
                         ((icmp-unreachable? buf) =>
                          (lambda (code)
                            (cond ((eq? mode 'dot)
                                   (define ch #\nul)
                                   (case code
                                     ((0) (set! ch "N"))
                                     ((1) (set! ch "H"))
                                     ((2) (set! ch "P"))
                                     ((4) (set! ch "F"))
                                     ((9 10 13) (set! ch "A"))
                                     ((11 12) (set! ch "O"))
                                     (else (set! ch "U")))
                                     (if quiet
                                       (set! dot-string
                                             (string-append dot-string ch))
                                       (begin
                                         (display ch)
                                         (if (and
                                               (> dotcols 0)
                                               (= 0 (modulo (+ 1 seq) dotcols)))
                                           (newline)
                                           (flush-output)))))
                                  ((eq? mode 'mtu)
                                   (set! double-mtu #f)
                                   (set! mtu-step (max 1 (fx/ mtu-step 2)))
                                   (set! size (max 40 mtu (- size mtu-step)))
                                   (set! repeated-pattern
                                         (create-pattern size pattern)))
                                  ((eq? mode 'stats)
                                   (unless quiet
                                     (print "icmp_seq="
                                            seq
                                            " dest unreachable: code["
                                            code
                                            "]"))))))
                         (else
                           (let ((icmp (icmp-decode buf)))
                             (cond ((eq? mode 'dot)
                                    (if quiet
                                      (set! dot-string
                                            (string-append dot-string "?"))
                                      (begin
                                        (display "?")
                                        (if (and
                                              (> dotcols 0)
                                              (= 0 (modulo (+ 1 seq) dotcols)))
                                          (newline)
                                          (flush-output)))))
                                   ((eq? mode 'stats)
                                    (unless quiet
                                      (print "icmp_seq="
                                             seq
                                             " type["
                                             (number->string (car icmp))
                                             "] code["
                                             (number->string (cdr icmp))
                                             "]"))))))))
                 (and (or (< (+ 1 seq) count) (= count 0))
                      (not (and (eq? mode 'probe)
                                recv-min
                                (= received recv-min)))
                      (not (and (eq? mode 'probe)
                                lost-max
                                (= (- sent received) lost-max)))
                      (not (and (eq? mode 'probe)
                                cons-lost-max
                                (= c-lost cons-lost-max)))
                      (not (and (eq? mode 'probe)
                                cons-recv-min
                                (= c-received cons-recv-min)))
                      (not (and (eq? mode 'mtu)
                                (= mtu-hits 5)
                                (= size mtu)))
                      (thread-sleep! (/ (- interval rtt) 1000))
                      (Loop (modulo (+ 1 seq) 65536) #f #f replied 0 0 0)))
                (else
                  ;; timeout occurred
                  (if prev-replied
                    (set! c-lost 1)
                    (set! c-lost (+ 1 c-lost)))
                  (cond ((eq? mode 'dot)
                         (if quiet
                           (set! dot-string (string-append dot-string "!"))
                           (begin
                             (display "!")
                             (if (and (> dotcols 0)
                                      (= 0 (modulo (+ 1 seq) dotcols)))
                               (newline)
                               (flush-output)))))
                        ((eq? mode 'mtu)
                         (set! double-mtu #f)
                         (set! mtu-step (max 1 (fx/ mtu-step 2)))
                         (set! size (max 40 mtu (- size mtu-step)))
                         (set! repeated-pattern (create-pattern size pattern)))
                        ((eq? mode 'stats)
                         (unless quiet
                           (print "Request timeout for icmp_seq=" seq))))
                  (and (or (< (+ 1 seq) count) (= count 0))
                       (not (and (eq? mode 'probe)
                                 lost-max
                                 (= (- sent received) lost-max)))
                       (not (and (eq? mode 'probe)
                                 cons-lost-max
                                 (= c-lost cons-lost-max)))
                       (not (and (eq? mode 'mtu)
                                 (= mtu-hits 5)
                                 (= size mtu)))
                       (thread-sleep! (/ (- interval timeout) 1000))
                       (Loop (modulo (+ 1 seq) 65536) #f #f replied 0 0 0)))))
        ;; exit from main loop
        (socket-close so)
        (cond ((eq? mode 'probe)
               (if (or (and recv-min (= recv-min received))
                       (and cons-recv-min (= c-received cons-recv-min))
                       (and lost-max (> lost-max (- sent received)))
                       (and cons-lost-max (> cons-lost-max c-lost)))
                 (begin (unless quiet (display msg1)) #t)
                 (begin (unless quiet (display msg2)) #f)))
             ((eq? mode 'dot)
              (if quiet
                dot-string
                (if (or (= dotcols 0) (not (= 0 (modulo count dotcols))))
                  (newline))))
              ((eq? mode 'mtu)
               (if (< sent count)
                 (begin
                   (unless quiet (print "mtu is " (+ mtu 28)))
                   (+ mtu 28))
                 (begin
                   (unless quiet (print "mtu discovery failed"))
                   #f)))
              ((eq? mode 'stats)
               (let* ((lost (- sent received))
                      (vstats (vector sent lost 0 0 0 0)))
                 (unless quiet (print-stats-rcv host received sent))
                 (and-let* (((> received 0))
                            (rtt-avg (/ rtt-tot received))
                            (rtt-std (sqrt (- (/  rtt2-tot received)
                                              (* rtt-avg rtt-avg)))))
                   (set! vstats
                         (vector sent lost rtt-min rtt-avg rtt-max rtt-std))
                   (unless quiet
                     (print-stats-rtt rtt-min rtt-avg rtt-max rtt-std)))
                 vstats)))))))
;;;
