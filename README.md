# eping

**eping** is a ([Chicken](http://www.call-cc.org)) Scheme implementation of the
ICMP ping utility. Although it can be used like an average OS ping utility,
**eping** is intended to be used programmatically (e.g. for gathering
statistics, monitoring). It uses raw sockets hence a privileged account must be
used.

```
[procedure] (eping HOST #!key (mode 'probe)
                              (count #f)
                              (timeout 1000)
                              (interval 1000)
                              (ttl 64)
                              (tos 0)
                              (dont-fragment #f)
                              (src-addr #f)
                              (id #f)
                              (seq-start 0)
                              (size 56)
                              (pattern "")
                              (recv-min #f)
                              (lost-max #f)
                              (cons-recv-min #f)
                              (cons-lost-max #f)
                              (msg1 "%i is alive%n")
                              (msg2 "")
                              (dotcols 0)
                              (quiet #t))
```

HOST can be either an IPv4 address or an hostname: "192.168.1.1",
"example.com", and 'example.com are all valid hosts.

**eping** supports four modes of operation: *probe* (default), *stats*, *dot* and
*mtu*.

## Index

1. [Probe mode](#markdown-header-probe-mode)
2. [Stats mode](#markdown-header-stats-mode)
3. [Dot mode](#markdown-header-dot-mode)
4. [Mtu mode](#markdown-header-mtu-mode)
5. [Parameters](#markdown-header-parameters)
6. [About this software](#markdown-header-about-this-software)
    * [Version history](#markdown-header-version-history)
    * [Author](#markdown-header-author)
    * [License](#markdown-header-license)

## Probe mode

In this mode **eping** returns *true* if it considers an host alive or *false*
otherwise. The default behavior is to consider an host alive as soon as an 
echo reply is received. If no reply is received in *count* attempts (5 by default),
the host is considered unreachable. As  *probe* mode is the default one, it can 
be omitted as parameter.

```
#!scheme
(eping "call-cc.org")

===> #t
```

The aliveness criteria can be changed through the five parameters *count*, 
*recv-min*, *lost-max*, *cons-recv-min*, *cons-lost-max* (see "Parameters" section). 
As soon as one of these conditions is met, **eping** returns the associated boolean
value.  If no condition is met in *count* attempts *#f* is returned.

In non-quiet 'submode' an informative message is displayed.

```
#!scheme
(eping "call-cc.org" quiet: #f)
209.172.49.65 is alive
```

The output message can be customized through the *msg1* and *msg2* parameters.

## Stats mode

In stats mode **eping** returns a vector of statistics *#(sent lost rtt-min
rtt-avg rtt-max rtt-std)*. The rtt values are expressed in *ms*.

```
#!scheme
(eping "call-cc.org" mode: 'stats)

===> #(5 0 133 134.2 136 0.92)
```

The non-quiet submode output resembles a standard ping utility:

```
#!scheme
(eping "call-cc.org" mode: 'stats quiet: #f)
64 bytes from 209.172.49.65: icmp_seq=0 ttl=52 time=139ms
64 bytes from 209.172.49.65: icmp_seq=1 ttl=52 time=227ms
64 bytes from 209.172.49.65: icmp_seq=2 ttl=52 time=135ms
64 bytes from 209.172.49.65: icmp_seq=3 ttl=52 time=170ms
64 bytes from 209.172.49.65: icmp_seq=4 ttl=52 time=256ms

--- call-cc.org ping statistics ---
5 packets transmitted, 5 packets received, 0% packet loss
round-trip min/avg/max/stddev = 135.0/185.4/256.0/48.26 ms
```

*Ctrl-\\* (POSIX) or *Ctrl-Break* (Windows) can be used to print partial statistics
without halting the process. *Ctrl-C* quits the ping process.

```
#!scheme
(eping "call-cc.org" mode: 'stats quiet: #f count: 0)
Request timeout for icmp_seq=0
64 bytes from 209.172.49.65: icmp_seq=1 ttl=52 time=299ms
64 bytes from 209.172.49.65: icmp_seq=2 ttl=52 time=151ms
64 bytes from 209.172.49.65: icmp_seq=3 ttl=52 time=288ms
64 bytes from 209.172.49.65: icmp_seq=4 ttl=52 time=434ms
64 bytes from 209.172.49.65: icmp_seq=5 ttl=52 time=668ms
64 bytes from 209.172.49.65: icmp_seq=6 ttl=52 time=349ms
^\
--- call-cc.org ping statistics ---
7 packets transmitted, 6 packets received, 14.28% packet loss
round-trip min/avg/max/stddev = 151.0/364.83/668.0/159.63 ms

64 bytes from 209.172.49.65: icmp_seq=7 ttl=52 time=705ms
64 bytes from 209.172.49.65: icmp_seq=8 ttl=52 time=143ms
64 bytes from 209.172.49.65: icmp_seq=9 ttl=52 time=142ms
64 bytes from 209.172.49.65: icmp_seq=10 ttl=52 time=156ms
64 bytes from 209.172.49.65: icmp_seq=11 ttl=52 time=202ms
^C64 bytes from 209.172.49.65: icmp_seq=12 ttl=52 time=147ms

--- call-cc.org ping statistics ---
13 packets transmitted, 12 packets received, 7.69% packet loss
round-trip min/avg/max/stddev = 142.0/307/705.0/192.55 ms
```

By default 5 packets are sent. The *count* option can be used to change this
value (0 meaning continuos operation).

## Dot mode

In this mode **eping** returns a single character for each echo request:

- [.] Echo reply received
- [!] No response
- [N] Network unreachable
- [H] Host unreachable
- [P] Protocol unreachable
- [F] Packet too big and DF bit set
- [A] Administratively filtered
- [T] Time to live exceeded
- [O] Network/Host unreachable for TOS
- [U] Unreachable (generic error)

```
#!scheme
(eping "call-cc.org" mode: 'dot quiet: #f timeout: 134 count: 20)
..!...!....!.!!!....
```

The *dotcols* parameter can be used to set the number of chars to be printed
before a newline. The default behavior (0) inserts a newline only after the
last echo request.

```
#!scheme
(eping "call-cc.org mode": 'dot quiet: #f timeout: 134 count: 20 dotcols: 5)
!...!
....!
...!.
!....
```

In quiet 'submode' a string built from the generated characters is returned.

```
#!scheme
(eping "call-cc.org" mode: 'dot count: 3)
"..."
```

## MTU mode

In mtu mode **eping** attempts to discover the MTU (Maximum Transmission Unit)
along the path. To speed up the process it is reccomended to lower the default
1000*ms* echo requests inter-packet time (*interval* option).

```
#!scheme
(eping "call-cc.org" mode: 'mtu interval: 250)

===> 1492
```

Lossy links may slow down or even prevent the convergence. The *count* option
can be used to increase the number of attempts (default is 64). If the MTU
can't be computed *#f* is returned.

## Parameters

### General

* **count** The number of echo requests to be sent. The default is 5 for modes
  other than *mtu* and 64 for this mode. If set to 0 echo requests are sent
  until a probe condition is met (probe mode), MTU is computed (mtu mode),
  or SIGINT (Ctrl-C) is received (all modes).

* **timeout** The amount of time in *ms* to let pass before considering an host
  unreachable (default is 1000ms).

* **interval** The interval in *ms* between echo requests (default is
  1000ms). This value can't be set lower than *timeout* (this rule is enforced
  lowering *timeout* in case).

### IP

* **ttl** / **tos** Respectively the Time To Live and TOS in the IP header.

* **dont-fragment** Sets the Don't Fragment (DF) flag in the IP header.

* **src-addr** Binds the ICMP socket to a specific IP address.

### ICMP

* **size** The ICMP payload size.

* **pattern** A string used to fill the payload (repeated if necessary).

* **id** The Identifier field (0-65535). By default this value is generated
  randomly for every invocation of the eping function.

* **seq-start** The initial sequence of the echo requests (0 by default).

### Probing

* **recv-min** Minimum number of received replies before considering an host
  alive.

* **lost-max** Maximum number of missed replies before considering an host
  unreachable.

* **cons-recv-min** Minimum number of consecutive received replies before
  considering an host alive.

* **cons-lost-max**  Maximum number of consecutive missed replies before
  considering an host unreachable.

### Output

* **msg1** The message displayed when an host is considered alive. The default
  message is "%i is alive%n" where %i is expanded to the IP address of the
  host. Similarly %h is expadended to the hostname while %n and %t insert
  respectively a newline and a tab.

* **msg2**  The message displayed when an host is considered unreachable. By
  default is an empty string.

* **dotcols** In *dot* mode the number of displayed chars before a newline is
  inserted. By default (0) a newline is inserted only after the last displayed
  char.

* **quiet** By default eping doesn't display anything unless this option is set
  to *#f*.

## About this software

The source code is hosted at [Bitbucket](https://bitbucket.org/miklam/eping/).
Feel free to send pull requests or open an issue there. Alternatively, send an
e-mail to the [chicken-users](mailto:chicken-users@nongnu.org) mailing list for
information or requests.

### Author

[Michele La Monaca](mailto:eping@lamonaca.net)

### Version history

**1.0** Initial release

### License

```
Copyright (c) 2013-2014
Michele La Monaca (eping@lamonaca.net)
All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:

 1) Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 2) Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.

 3) Neither the name of the author nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
```